package com.example.myapplication

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text
import java.lang.Exception

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        findViewById<Button>(R.id.activity2Button).setOnClickListener {
            val newColor = findViewById<EditText>(R.id.inputColor).text.toString()

            val updatedColor = convertColor(newColor)

            if(updatedColor == -1) {
                Toast.makeText(applicationContext, "Enter A Valid Color", Toast.LENGTH_LONG).show()
            }
            else {
                changeColor(updatedColor)
            }
        }

    }

    fun convertColor(newColor: String): Int {
        var currentColor = -1

        try {
            currentColor = Color.parseColor(newColor)
        } catch (e: Exception) {
            Log.e("String",e.message ?: "hijhkjhj")
        }

        return currentColor
    }

    fun changeColor(updatedColor: Int) {
        findViewById<TextView>(R.id.activity2Title).setBackgroundColor(updatedColor)
    }
}